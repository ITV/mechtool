#!/usr/bin/env python3
#tool for reaction flux analysis

import re 
import networkx as nx
import operator
import os
import graphviz as gv
import numpy as np 
import pandas as pd
import nested_dict as nd


class Kinetics: 
   
    def __init__(self): 
        self.species_list = [] 
        self.reactions_list = []
        self.checked_species = [] 
        self.GlobalPath = [] 
        self.CommonIndex = {} 
        self.CommonPath = {}  
        self.CommonWeight = {}
        self.GlobWeight = {}  
          

    def add_species(self,newspecies):
        self.species_list.append(newspecies)  
           
    def add_reactions(self,reac): 
        self.reactions_list.append(reac)   

    def get_reactions_list(self):  
        return self.reactions_list 

    def get_species_list(self):
        return self.species_list

    def return_spec(self,name_spec):
        for elemen in self.species_list: 
            if elemen.species_name == name_spec:   
               return elemen
  
    def return_case(self,case_id):
        for elemen in self.GlobalPath: 
            if elemen.id == name_spec:   
               return elemen  


class Species:
       
    def __init__(self, name):
        self.id = name 
        self.species_name = name  
        self.species_index = 0 
        self.mola_mass = 0 
        self.species_comp = {} 
        self.species_adjacent = nd.nested_dict(2,tuple)  
        self.spec_reactions={}
        self.side_temp = {} 
        self.side_s = {} 
        self.reaction_flux = {} 
        self.rf = {} 
        self.species_adjacent_rf = {}
        self.species_adjacent_rf_abs = {}
        self.normFac = 0   
        self.side_connect = {} 
        self.multFac = {} 

    def fill_species(self,atoms_Name,atoms_value,MM):
        for i in atoms_name:  
            self.species_comp[atoms_name[i]] = atoms_value[i] 
        self.speciesMolarMass = MM 
    
    def get_species_name(self,species_index):
        return self.species_name 
  
    def get_species_index(self,species_name): 
        return self.species_index  

    def add_neighbor(self, neighbor_spec , label="",net_prod = 0,net_cons = 0):
        self.species_adjacent[neighbor_spec][label] = (net_prod,net_cons)


    def add_side_s(self,label,side_sp=[]):
        self.side_temp[label] =side_sp 

    def add_side_rf(self,neigh,k):
        neigh = list(neigh) 
        label = neigh.pop()  
        for elem in neigh:
            side_spec = k.return_spec(elem)  
            if elem in self.side_s.keys():
               curr_list = self.side_s[elem]
               if label in side_spec.side_temp.keys(): 
                  f_list = list(set(curr_list + side_spec.side_temp[label]))
                  self.side_s[elem] = f_list
            else:
                if label in side_spec.side_temp.keys(): 
                   self.side_s[elem] = side_spec.side_temp[label]

    def add_neighbor_rf(self, neighbor_spec,net_val = 0):
        neighbor_spec = list(neighbor_spec) 
        neighbor_spec.pop() 
        for elem in neighbor_spec: 
            if elem in self.species_adjacent_rf.keys(): 
               curr_weight = self.species_adjacent_rf[elem] 
               w = net_val + curr_weight  
               self.species_adjacent_rf[elem] = w
            else: 
               self.species_adjacent_rf[elem] = net_val
  
    def add_neighbor_rf_abs(self, neighbor_spec,net_val = 0):
        neighbor_spec = list(neighbor_spec) 
        neighbor_spec.pop() 
        for elem in neighbor_spec: 
            if elem in self.species_adjacent_rf.keys(): 
               curr_weight = self.species_adjacent_rf[elem] 
               w = net_val + curr_weight  
               self.species_adjacent_rf_abs[elem] = w
            else: 
               self.species_adjacent_rf_abs[elem] = net_val 


    def get_weight_graph(self,adj):

        temp_list = [] 
        for elem in self.rf.keys():
            list_of_elem = list(elem)
            reac = list_of_elem.pop()
            
            if adj in list_of_elem: 
               list_of_elem.remove(adj)
               tup = (reac,self.rf[elem])  
               temp_list.append(tup) 

        return temp_list
 
    def get_singlereac_weight(self,reac_name):
        for elem in self.rf.keys():
            list_of_elem = list(elem)
            reac = list_of_elem.pop()
            if reac == reac_name: 
               return self.rf[elem]  
            
    def get_getNodeAdj_reac(self,adj):
        temp_list = [] 
        weight = 0; 
        for elem in self.rf.keys():
            list_of_elem = list(elem)
            reac = list_of_elem.pop()
            
            if adj in list_of_elem: 
               list_of_elem.remove(adj)
               tup = (reac,self.rf[elem])  
               temp_list.append(reac) 
               weight = weight + self.rf[elem] 

        return temp_list,weight  

    def add_reactionflux(self,reac_label,listspec = [],net_prod = 0, net_cons = 0):
        listspec.append(reac_label) 
        tup = tuple(listspec) 
        self.reaction_flux[(tup)] = (net_prod,net_cons) 

    def add_specreac(self,label,reac,net_prod,net_cons):
        self.spec_reactions[label] = (reac,net_prod,net_cons)


    def get_normFac(self): 
        PA = 0.0 
        CA = 0.0 
        for items in self.spec_reactions.keys():
            PA = PA + max(0,self.spec_reactions[items][1])  
            CA = CA + max(0,self.spec_reactions[items][2])
       
        self.normFac = max(PA,CA)
        return PA,CA 
 
    def get_connections(self):
        return self.species_adjacent.keys()  



class Reaction:
      def __init__(self,label): 
          self.id = label 	 
          self.isrev = False 
          self.isforw = False  
          self.reactants = {} 
          self.products = {} 
          self.thirdbody = ""    
          self.intProdRate = 0
          self.isback = False   
          self.backreaclabel = "" 

      def add_reaction(self,isrev,isforw,isback,reacs,stoichreac,products,stoichproducts,thirdbody): 
          self.isforw = isforw
          self.isrev = isrev
          self.isback = isback  
 
          if(self.isrev): 
            if (self.isback): 
               label=self.id.replace("b","f") 
               self.backreaclabel = label 

            elif (self.isforw):  
                label = self.id.replace("f","b") 
                self.backreaclabel = label 

          for i in range(0,len(reacs)): 
              self.add_reactant(reacs[i],stoichreac[i]) 

          for i in range(0,len(products)): 
              self.add_product(products[i],stoichproducts[i]) 
  
      def get_backreac(self,labelback):
          return Reaction(labelback) 

      def get_label(self): 
          return self.id 
 
      def add_reactant(self,reac,stoich):  
          self.reactants[reac] = -float(stoich)
 
      def add_product(self,prod,stoich):
           self.products[prod] = float(stoich)  

      def add_integratedReacRate (self,ProdRate):  
          self.intProdRate = ProdRate
   
def buildGraph(k,source,path=[],weight=[],w=0,l=0,ty="",s=[],compoFlag=False,compositionElem=[],compositionLimit=[],specBased=False,file_name='selected_species.txt'): 
    Graph = {}  

    for spec in k.species_list:
        list_adj = [] 
        for adj_spec in spec.species_adjacent_rf.keys(): 
            list_adj.append(adj_spec)

        Graph[spec.species_name] = set(list_adj) 

    all_paths = list(dfs_paths(k,Graph,source,l))          
    print("Pathways found\n") 
    g = nx.DiGraph()

    if re.search(r'all',s[0],re.I):
       kept_paths = all_paths.copy() 
    else:  
       kept_paths = []
       for i in range(0,len(all_paths)): 
           if all_paths[i][-1] in s:  
              kept_paths.append(all_paths[i])
         
    if compoFlag: 
       temp_path = kept_paths.copy() 
       for elem in temp_path: 
           flagList = [] 
           for i in range(len(compositionElem)): 
               if int(compositionLimit[i]) >= 0: 
                  rem = adjustBasedOnCompo(k,elem[-1],compositionElem[i],compositionLimit[i]) 
                  flagList.append(rem) 
           if not any(flagList): 
              kept_paths.remove(elem) 


    all_paths = sum(kept_paths,[])
    i = 0
    while i < (len(all_paths) - 1): 
        node_to_anal = k.return_spec(all_paths[i]) 
        adj = all_paths[i + 1]
        adj_spec = k.return_spec(adj)
        label = ""
        list_of_extra =[] 
        
        #Label with side species
        if ty == "p":  
           if adj in node_to_anal.side_s.keys():
              list_of_extra = node_to_anal.side_s[adj] 
              for elem in list_of_extra: 
                  label = label + "+" + str(elem) +"\n"
        else:  
         if node_to_anal.species_name in adj_spec.side_s.keys():
            list_of_extra = adj_spec.side_s[node_to_anal.species_name] 
            for elem in list_of_extra: 
                label = label + "+" + str(elem) +"\n" 
           
   
        if ty == "p":
           if adj != all_paths[0]:
              color_e = color_condition(k,k.return_spec(adj),node_to_anal,list_of_extra) 
              w = node_to_anal.species_adjacent_rf[adj]
              if w == 0 :                    
                 label = "" 
              else: 
                 label = label  + str(str(round(w*100,3)) + "%\n")

              if not g.has_edge(node_to_anal.species_name,adj):
                # g.add_edge(adj,node_to_anal.species_name,weight =w,label =str(str(round(w*100,3)) + "%\n"),penwidth=3 ) 
                 g.add_edge(node_to_anal.species_name,adj,weight =w,label =str(str(round(w*100,3)) + "%\n"),penwidth=3 ) 
        else:
           if adj != all_paths[0]:
              color_e = color_condition(k,node_to_anal,k.return_spec(adj),list_of_extra)
              w = node_to_anal.species_adjacent_rf[adj]
              if w == 0 :                    
                 label = "" 
              else: 
                 label = label  + str(str(round(w*100,3)) + "%\n")
 
              if not g.has_edge(node_to_anal.species_name,adj):
                 g.add_edge(node_to_anal.species_name,adj,weight = w, label =str(str(round(w*100,3)) + "%\n"),penwidth=3  ) 
                    
        i+=1
 
    #refine the graph with dot
    ng = g.copy()  

    if not specBased: 
        for node in g.nodes:
            start_node = k.return_spec(node) 
            weight_dic = {} 
            reac_dic = {}
            adj_node = [n for n in g.neighbors(node)]
            for adj in adj_node: 
                reac_dic[adj],weight_dic[adj] = start_node.get_getNodeAdj_reac(adj)
            refine(node,reac_dic,weight_dic,ng,k,ty) 
         
    
    else: 
       refine_2(k,ng) 

    if ty == "p":
           ng = nx.DiGraph.reverse(ng) 

    print("\nNodes in  your graph:") 
    print(list(g.nodes))

    content_array = np.array(list(g.nodes))
    np.savetxt(file_name, content_array, fmt='%s')

    print("Generating graph")
    ng.graph['graph']={'rankdir':'TD'} 
    ng.graph['node']={'shape':'plaintext'}
    ng.graph['edges']={'arrowsize':'4.0'} 
 
    ng=nx.nx_agraph.to_agraph(ng)
    ng.layout('dot')
    ng.draw('networkx_graph.png')
         
    return 0 


def refine_2(k,ng): 
    for node in ng.nodes(): 
        total_weight = 0  
        start_node = k.return_spec(node)
        print("start node: " + node) 
        total_weight = 0
        for key in start_node.species_adjacent_rf_abs.keys(): 
            total_weight = total_weight + start_node.species_adjacent_rf_abs[key]
        adj_node = [n for n in ng.neighbors(node)]
        for adj in adj_node: 
            new_weight = start_node.species_adjacent_rf_abs[adj]/total_weight
            ng.get_edge_data(node,adj)['weight']=new_weight 
            ng.get_edge_data(node,adj)['label'] = str(str(round(new_weight*100,3) )+ "%\n")

 
def refine(node,reac_dic,weight_dic,g,k,ty): 

    sorted_weight = dict(sorted(weight_dic.items(), key=operator.itemgetter(1),reverse=True))
    ABcouples = [] 
    start_node = k.return_spec(node)
    common_reac = {} 
 
    for key1 in sorted_weight.keys():
        nodeA_reac = reac_dic[key1]
        for key2 in sorted_weight.keys():
            if (key1,key2) not in ABcouples and key1 != key2: 
                #check if A and B share the same reactions
                nodeB_reac = reac_dic[key2]   
                common = list(set(nodeA_reac).intersection(nodeB_reac))
                for reac in common:
                    if reac in common_reac.keys():
                       common_reac[reac] =  common_reac[reac] + [key2] 
                       common_reac[reac] =  common_reac[reac] + [key1] 
                       common_reac[reac] = list(set(common_reac[reac]))
                    else:  
                       common_reac[reac] =  [key1,key2] 

 
    #find duplicates values in common reac 
    common_reac_sort = {}
    for key in common_reac.keys():
        if tuple(common_reac[key]) in common_reac_sort.keys():
           common_reac_sort[tuple(common_reac[key])].append(key)
        else:
           common_reac_sort[tuple(common_reac[key])] = [key]    

    #adjust weight in your graph
    #save dot name for later 
    dot = {}  
    for key in common_reac_sort.keys(): 
        weight = 0 
        for reac in common_reac_sort[key]: 
            weight = weight + start_node.get_singlereac_weight(reac)

        dotName = ''.join(key) 
        g.add_node(dotName,label="", shape="point", width=0.03, height=0.01)
        if ty == "c":         
           g.add_edge(node,dotName,arrowhead = "none",weight=round(weight*100,3),label = str(str(round(weight*100,3) )+ "%\n"),penwidth=3 )
        else:   
           g.add_edge(node,dotName,weight=round(weight*100,3),label = str(str(round(weight*100,3) )+ "%\n"),penwidth=3 )
 
        for spec in list(key): 
            #update weight by sub the common reaction. if = 0 then remove edge 
            weight1 = g.get_edge_data(node,spec)['weight']
            new_weight = weight1 - weight 
            if new_weight > 0:  
               g.get_edge_data(node,spec)['weight'] = new_weight 
               g.get_edge_data(node,spec)['label'] = str(str(round(new_weight*100,5) )+ "%\n") 
            else: 
               g.remove_edge(node,spec)
            
            if ty == "c": 
               g.add_edge(dotName,spec,penwidth=3)
            else: 
               g.add_edge(dotName,spec,arrowhead = "none",penwidth=3)
            dot[spec] = dotName   
   

    for key in dot: 
        if g.has_edge(node,key): 
           weight =  g.get_edge_data(node,key)['weight'] 
           g.remove_edge(dot[key],key)
           if ty == "c": 
              g.add_edge(dot[key],key, weight = weight, label = str(str(round(weight*100,3) )+ "%\n"), penwidth=5)
           else:  
              g.add_edge(dot[key],key,arrowhead = "none", weight = weight, label = str(str(round(weight*100,3) )+ "%\n"), penwidth=5)
           g.remove_edge(node,key) 

    return 0 
     
def dfs_paths(k,graph,source,l=0):
    ep = 1; lev = 0 ; inc_lev = 0; 
    stack = [(source,[source],ep,lev)]
    while stack: 
       (node,path,ep,lev) = stack.pop()
       spec = k.return_spec(node)
       if lev == 0 :
          inc_lev += 1  
          for next in graph[node] - set(path): 
              #stop when you are going too deep 
              if spec.species_adjacent_rf[next] > l :
                   stack.append((next,path + [next],ep*spec.species_adjacent_rf[next],inc_lev))
       
       elif lev > 0  :
            inc_lev += 1 
            for next in graph[node] - set(path): 
              #stop when you are going too deep 
                if ep*spec.species_adjacent_rf[next] > l :
                     stack.append((next,path + [next],ep*spec.species_adjacent_rf[next],inc_lev))
                     yield path + [next]
 
 
def color_condition(k,spec,adj,side): 

    color = "" ; color_list = []; color_adj = ""  
    if int(spec.species_comp["C"]) >= 1 and int(spec.species_comp["O"]) == 0  or color_adj == "red": 
       color = "black" 
    elif int(spec.species_comp["O"]) >= 1 or color_adj == "blue": 
       color = "black" 
    else: 
       color = "black" 
 
    return color 

def adjustBasedOnCompo(k,spec_name,key,limit):

    keep = False  
    spec = k.return_spec(spec_name)
    if int(spec.species_comp[key]) >= limit: 
       keep = True   
    return keep 

def readMechanism (k,mech):  

    flag_done=False
    try: 
     f = open(mech, 'r')
    except IOError:
     print("Couldn't open outscan file ")

    outscan = f.readlines()
    f.close 
 
    i = 0
    thirdbodieslist=[]  
    while (i < (len(outscan)-1)): 
            
    #store species 
           if ( re.match(r"The List \'Reactions\' has ([0-9]+) Items\.", outscan[i]) and flag_done == False):
                while (not re.match(r"The List \'Species\' has ([0-9]+) Items\.", outscan[i])):
                        i += 1
           if (outscan[i] and re.match(r"The List \'Species\' has ([0-9]+) Items\.", outscan[i])):
                i += 2
                flag_done=True
                while(re.match(r"^([\d\D]+) is species", outscan[i]) ):
                     name =  re.search(r"^([\d\D]+) is species no\. ([0-9]+)", outscan[i])
                     spec_index =  name.group(2)
                     spec_name =  name.group(1)
                     new_species = Species(spec_name) 
                     new_species.species_index = spec_index
                     i+=1 
                     if(re.match(r"steady state", outscan[i])): 
                        i +=1  
                     
                     if(re.findall(r"\s*([0-9]+)\s*([A-Za-z]+)\s*", outscan[i])): 
                          atom_list = re.findall(r"\s*([0-9]+)\s*([A-Za-z]+)\s*", outscan[i])
                          for items in atom_list: 
                              new_species.species_comp[items[1]]=items[0]  
                          
                          i+=2    
                     if (re.search(r"M = ([0-9eE\+\-\.]+)\s",outscan[i])):  
                         mol = re.search(r"M = ([0-9eE\+\-\.]+)\s",outscan[i])
                         new_species.mol_mass = mol.group(1) 
                          
                     
                     k.add_species(new_species)                                
                     i += 11    
              
    #store thirdbodies 
           if (re.match(r"The List \'Third Body Coefficients\' has ([0-9]+) Items\.",outscan[i])): 
               i+=2 
               
               while(re.match(r"^Nr[\d\D]+:\s*\[(M[\d\D]*)\]\s*=([\d\D]+)",outscan[i])): 
                     tb = re.search(r"^Nr[\d\D]+:\s*\[(M[\d\D]*)\]\s*=([\d\D]+)", outscan[i]) 
                     thirdbodieslist.append(tb.group(1))
                    
                     while(re.search(r"\+",outscan[i+1])):
                           i+=1
                     i+=1  
                     while(not bool(re.match(r"[a-z0-9]",outscan[i],re.IGNORECASE))and i < (len(outscan) -1)):
                           i+=1 

    #store reactions 
           if (outscan[i] and re.match(r"The List \'Reactions\' has ([0-9]+) Items\.", outscan[i]) and flag_done):
               i += 1 
               while(re.search(r"\"([\d\D]+)\"",outscan[i])):
                     label = re.search(r"\"([\d\D]+)\"",outscan[i]) 
                     label = label.group(1)
                     new_reac = Reaction(label) 

                     i+=2 
                     isrev = False;isforw = False;isback = False;  
      
                     if re.search(r"f$",label):  
                         isforw = True ;isrev = True    
                     elif (re.search(r"b$",label)):
                         isback = True ; isrev = True  

                     eq = outscan[i] ; eq = eq.strip();  
                     eq = eq.split(" ")  
                      
                     flag = 0;thirdbody="";reac = []; stochreac = [];prod = [];stochprod = []; 
                    
                     j=0 
                     while(j < (len(eq)-1)):
                         if (re.match(r"->", eq[j])): 
                             flag = 1  
                             j+=1   
                         elif(re.match(r"\+",eq[j])): 
                             j+=1    
                        
                         if(eq[j] in thirdbodieslist): 
                              if (flag == 0):
                                  thirdbody = eq[j] 
                         elif (re.match(r"^[0-9\-\+\.eE]+$",eq[j])):
                              j+=1 
                              if(flag == 1):
                                 prod.append(eq[j])  
                                 stochprod.append(eq[j-1])
                              else:
                                 reac.append(eq[j]) 
                                 stochreac.append(eq[j-1])
                                  
                         else: 
                              if(flag == 1):
                                prod.append(eq[j])
                                stochprod.append(1) 
                              else:
                                reac.append(eq[j]) 
                                stochreac.append(1) 
                         j+=1 
                         
                     new_reac.add_reaction(isrev,isforw,isback,reac,stochreac,prod,stochprod,thirdbody) 
                     k.add_reactions(new_reac) 

                     while (not bool(re.search(r"\"([\d\D]+)\"",outscan[i])) and i < (len(outscan) -1) ): 
                           i+=1                                  
           i+=1  
    return 0             
                               
def Link(reacratefile,k,ty,index,specBased): 
    df = pd.read_csv(reacratefile,sep = "\t",skiprows=1 )
    time = df.iloc[:,0].tolist()
    others = []  
   
    
    #doing link between species 
    for spec in k.species_list: 
        if (spec.species_name not in others): 
            for reaction in k.reactions_list:
               reacs = reaction.reactants.keys() 
               prods =  reaction.products.keys() 
               #check if the reaction should be actually considered 
               Flag_reac = True 
               if spec.species_name in reacs and spec.species_name in prods: 
                  if abs(reaction.reactants[spec.species_name]) == abs(reaction.products[spec.species_name]): 
                     Flag_reac = False
               if Flag_reac:     
                   if (reaction.isforw):
                       if (spec.species_name in reacs):
                         
                          side_s = [] 
                          for reac in reacs: 
                              if reac != spec.species_name: 
                                 side_s.append(reac)  
    
                          spec.add_side_s(reaction.id,side_s) 
                          net_cons = compute_net(reaction,-reaction.reactants[spec.species_name],df,time)
                          net_prod = compute_net(reaction,reaction.reactants[spec.species_name],df,time)
                          spec.add_specreac(reaction.id,reaction,net_prod,net_cons)
                          lp = []   
                          for prod in prods: 
                              if (prod not in others):
                                  spec.add_neighbor(prod,reaction.id,net_prod,net_cons)
                                  lp.append(prod)
                          spec.add_reactionflux(reaction.id,lp,net_prod,net_cons)
    
                       elif (spec.species_name in prods):
    
                             side_s = [] 
                             for prod in prods: 
                                 if prod != spec.species_name: 
                                    side_s.append(prod) 
    
                             spec.add_side_s(reaction.id,side_s) 
                             net_cons= compute_net(reaction,-reaction.products[spec.species_name],df,time)
                             net_prod = compute_net(reaction,reaction.products[spec.species_name],df,time)
                             spec.add_specreac(reaction.id,reaction,net_prod,net_cons)  
                             lr = [] 
                             for reac in reacs: 
                                 if (reac not in others): 
                                     spec.add_neighbor(prod,reaction.id,net_prod,net_cons)
                                     lr.append(reac)
                             spec.add_reactionflux(reaction.id,lr,net_prod,net_cons)
    
                   elif not reaction.isrev:
                        if (spec.species_name in reacs) :
                            
                            side_s = [] 
                            for reac in reacs: 
                                if reac != spec.species_name: 
                                   side_s.append(reac) 
                            spec.add_side_s(reaction.id,side_s) 
                            net_cons = compute_net(reaction,-reaction.reactants[spec.species_name],df,time)
                            net_prod = compute_net(reaction,reaction.reactants[spec.species_name],df,time)
                            spec.add_specreac(reaction.id,reaction,net_prod,net_cons) 
                            lp = []  
                            for prod in prods: 
                                if (prod not in others): 
                                    spec.add_neighbor(prod,reaction.id,net_prod,net_cons)
                                    lp.append(prod) 
                            spec.add_reactionflux(reaction.id,lp,net_prod,net_cons)
    
                        elif (spec.species_name in prods):
     
                              side_s = [] 
                              for prod in prods: 
                                  if prod != spec.species_name: 
                                     side_s.append(prod) 
    
                              spec.add_side_s(reaction.id,side_s) 
                              net_cons = compute_net(reaction,-reaction.products[spec.species_name],df,time)
                              net_prod = compute_net(reaction,reaction.products[spec.species_name],df,time)
                              spec.add_specreac(reaction.id,reaction,net_prod,net_cons)
                              lr = []   
                              for reac in reacs: 
                                  if (reac not in others): 
                                      spec.add_neighbor(reac,reaction.id,net_prod,net_cons)
                                      lr.append(reac) 
                              spec.add_reactionflux(reaction.id,lr,net_prod,net_cons)
    
    
    #Create a directory with RFA text file 
    working_dir = os.getcwd() 

    #file name 
    file_name = str("RFA_" + reacratefile + str(index))
    fout = open(file_name,"w") 
    
    for spec in k.species_list:
        negl= 0.0 
        if (spec.species_name not in others):
           fout.write(str("Starting spec " + spec.species_name)) 
           
           PA,CA = spec.get_normFac() 
           norm_fac = spec.normFac 
                
           if ty == "p":    
              fout.write(str(" Tot production of species " + spec.species_name + " is " + str(PA) + "\n")) 
           else:  
              fout.write(str(" Tot consumption  of species " + spec.species_name + " is " + str(CA) + "\n")) 

           #ReactionFlux
           tot_cons = 0; tot_prod = 0;     
           for items in spec.reaction_flux.keys():
               tot_cons = max(0,spec.reaction_flux[items][1])
               tot_prod = max(0,spec.reaction_flux[items][0])
               
               if CA != 0.0:
                  abs_cons = tot_cons 
                  tot_cons = tot_cons/CA 
               else: 
                  abs_cons = 0.0 
                  tot_cons = 0.0  
               if PA != 0.0:
                  abs_prod = tot_prod 
                  tot_prod = tot_prod/PA
               else: 
                  abs_prod=0.0
                  tot_prod = 0.0 
               
               if ty == "p": 
                  if (round(tot_prod*100,3) > 0):
                      spec.add_neighbor_rf(items,tot_prod)
                      spec.add_neighbor_rf_abs(items,abs_prod)
                      if (round(tot_prod*100,3) > 1):
                          spec.add_side_rf(items,k)
                      fout.write(str(str(round(tot_prod*100,3)) + "% of Species " + spec.species_name +   " is produced by " + str(items) + " via reac " + str(items[-1]) + "\n" ))
                      spec.rf[items] = tot_prod
               else: 
                   if (round(tot_cons*100,3) > 0):
                      spec.add_neighbor_rf(items,tot_cons)
                      spec.add_neighbor_rf_abs(items,abs_cons)
                      if (round(tot_cons*100,3) > 1):
                         spec.add_side_rf(items,k)
                      fout.write(str(str(round(tot_cons*100,3)) + "% of Species " + spec.species_name +   " goes to " + str(items) + " via reac " + str(items[-1]) + "\n" ))
                      spec.rf[items] = tot_cons  
     
    fout.close() 
    return 0 


def compute_net(reac,stoch,df,time): 
    
    integrated = 0.0 
    
    if (reac.isrev):
       df.loc[df[reac.id] == 1.0e-15 , reac.id] = 0.0 
       df.loc[df[reac.backreaclabel] == 1.0e-15 ,reac.backreaclabel] = 0.0  
       wf = df[reac.id] 
       wb = df[reac.backreaclabel]
       df["net"] = stoch * (wf - wb)   
       net = df["net"].tolist() 
     
       integrated = integrate_net(net,time,0.0,2) 
       temp = [integrated[0]]
       temp.extend([integrated[i]/(time[i] - time[0]) for i in range(1,(len(time)))]) 
       integrated_val = temp[-1]
       
    else: 
       df.loc[df[reac.id] == 1.0e-15 , reac.id] = 0 
       w = df[reac.id] 
       df["net"] = stoch * w 
       net = df["net"].tolist()

       integrated = integrate_net(net,time,0.0,2) 
       temp = [integrated[0]]
       temp.extend([integrated[i]/(time[i] - time[0]) for i in range(1,(len(time)))]) 
       integrated_val = temp[-1]
       
    
    return integrated_val     

            
def integrate_net(y,x,init,m):
    init = float(init)
    if (m==2):
        I = [0]*len(x)
        I[0] = init
        I[1] = I[0] + y[1]*(x[1]-x[0]) 
        for j in range(2,len(x)):
            h1 = x[j] - x[j-1]
            h2 = x[j-1] - x[j-2]
            if (h1 <= 1.0e-16 or h2 <= 1.0e-16):
                I[j] = I[j-1] + y[j]*(x[j] - x[j-1]) 
            else :
                fac = (h1 + h2)/h1
                N = y[j]*fac*h2 + I[j-1]*fac*fac - I[j-2]
                D = fac*fac-1
                I[j] = N/D 
    elif (m==3):
        I = [0]*len(x)
        I[0] = init
        I[1] = I[0] + y[1]*(x[1]-x[0]) 
        h1 = x[2] - x[1]
        h2 = x[1] - x[0]
        fac = (h1 + h2)/h1
        N = y[2]*fac*h2 + I[1]*fac*fac - I[0]
        D = fac*fac-1
        I[2] = N/D 
        for j in range(3,len(x)):
            h1 = x[j] - x[j-1]
            h2 = x[j-1] - x[j-2]
            h3 = x[j-2] - x[j-3]
            if (h1 == 0 or h2 == 0 or h3 == 0): 
                I[j] = I[j-1] + y[j]*(x[j] - x[j-1])  
            else :
                lam = (h1 + h2)/h1
                eps = (h1 + h2 + h3)/h1
                p = 0.5*h1*h1*lam*lam*(lam-1)
                q = 0.5*h1*h1*eps*eps*(eps-1)
                N = y[j]*(h1*((lam*lam*lam-1)*q - (eps*eps*eps-1)*p) - h2*(q-p) + h3*p)  +  I[j-1]*(lam*lam*lam*q - eps*eps*eps*p)  -  I[j-2]*q  + I[j-3]*p
                D = (lam*lam*lam-1)*q - (eps*eps*eps-1)*p
                I[j] = N/D
    else :
        I = [0]*len(x)
        I[0] = init
        for j in range(1,len(x)):
            dt = x[j] - x[j-1]
            I[j] = I[j-1] + y[j]*dt
    return I

    














