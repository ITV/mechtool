#!/usr/bin/env python3


import ReacFlux as rf 
import os 
import sys 
import getopt

def handle_command_line(argv): 
    
    target = "" 
    sources = ""
    outscan = "" 
    reacfile = ""
    ty = "" 
    limit = 0
    allFolder = "False" 
    species_file = "" 

    arg_help = "{0} -i <outscan> -r <reac_rate> -t <source> -f <type> -l <limit> -a <folder> -w <species_file>".format(argv[0]) 

    try:
        opts, args = getopt.getopt(argv[1:], "hi:r:s:t:l:f:w:")
    except:
        print(arg_help)
        sys.exit(2)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print("This script computes the net integrated production or consumption rate\n")  # print the help message
            print("Options:\n") 
            print(" -h help\n -i outscan\n -r reac rates file\n -e endPoint\n -f c/p\n -l limit\n -s start\n -a Folder\n -w Species_filename") 
            sys.exit(2)
        elif opt in ("-i", "--input"):
            outscan = arg
        elif opt in ("-r", "--reacfile"):
            reacfile = arg
        elif opt in ("-s", "--source"):
            sources = arg 
        elif opt in ("-t", "--target"): 
            target = arg
        elif opt in ("-f", "--format"): 
             ty = arg 
        elif opt in ("-l", "--limit"): 
            limit = float(arg)    
        elif opt in ("-l", "--limit"): 
            allFolder = arg    
        elif opt in ("-w", "--species_file"): 
            species_file = arg    


    print('Input:', outscan)
    print('Reacfile:', reacfile)
    print('Target:', target)
    print('Sources:', sources)
    print('Type:', ty)
    print('Limit:', limit)
    print('Species file name:', species_file)


    return target, ty, reacfile, outscan, limit, sources, allFolder, species_file

if __name__ == "__main__": 
      
     target, ty, reacfile, outscan, limit, sources, allFolder, species_file = handle_command_line(sys.argv)
     checkCompo=True
     composition_array=['C','H','O','N','AR','HE'] 
     composition_limit = [1,-1,-1,-1,-1,-1]
     specBased=True 
     if "," in sources: 
         sources = sources.strip();  
         sources = sources.replace(",", " ")
         sources = sources.split(" ")
     else:
         print(str("\nOnly one source is provided "  + sources)) 
         sources =[sources]  
      

     k = rf.Kinetics() 
     rf.readMechanism(k,outscan) 
 
     rf.Link(reacfile,k,ty,0,specBased)
     rf.buildGraph(k,target,path=[],weight=[],w=0,l=limit,ty=ty,s=sources,compoFlag=checkCompo,compositionElem=composition_array,compositionLimit=composition_limit,specBased=specBased,file_name=species_file) 




