#Updated Script for Mechanism parsing
#Author: Laurenz? 
#Revised: Francesca Loffredo  

# Import Modules
import shutil
import re
import os
from ParsingFunc import *

#########################################################################################################################################################

# Input
itvFullMechPath  = '/rwthfs/rz/cluster/work/rm205731/MechanismParser/Input/ITV_Mech/ITV_Full_mech.mech'
itvMechPath      = '/rwthfs/rz/cluster/work/rm205731/MechanismParser/Output/Mechanism/OME1_3.mech'
itvThermoPath    = '/rwthfs/rz/cluster/work/rm205731/MechanismParser/Output/Mechanism/OME1_3.thermo' 
itvTransPath     = '/rwthfs/rz/cluster/work/rm205731/MechanismParser/Output/Mechanism/OME1_3.trans' 
mech_name_input  = 'OME1_3'
mech_name        = 'OME1_4'

ExtraMechPath    = '/rwthfs/rz/cluster/work/rm205731/MechanismParser/Input/OME4/OME4136.mech'
ExtraThermoPath  = '/rwthfs/rz/cluster/work/rm205731/MechanismParser/Input/OME1/OME175.thermo'
ExtraTransPath   = '/rwthfs/rz/cluster/work/rm205731/MechanismParser/Input/OME1/OME175.thermo' 
ExtraInChIPath   = '/rwthfs/rz/cluster/work/rm205731/MechanismParser/Input/NOxGl/InChIDictExtra.txt'

#No Inchi 
noInc=True 

#Extraction based on SearchList. The List of species to be added can be defined based on:  
#Explicit list of species to add 
searchListPath   = ''
#List of species containing a specific name in their name   
searchWord       = ''
#Difference between species in two mechanisms
flagCompare      = True  

#To select if you want to define deprecated species based on those present in ITV full mechanism. Useful when handling reduced mechanisms.  
flagITVFull      = False
#Dummy mech: when you want to extract reactions which contain specific species 
DummyMech        = False  

#Search of "Second Generation species". With Recursive 
writeFiles       = True
Recursive        = False
setIterNum       = -1  

#Select if a new thermo file has to be written. Set this to False when manipulating two reduced version of the ITV_mech 
updateThermo     = True 

#########################################################################################################################################################
# Check if Paths Have Been Set
if ExtraMechPath == '' or itvMechPath == '':
    print('ERROR: Paths to Extra Mechanisms have not been set. Please set the Paths accordingly in the MechanismParse.py script.')
    quit()
# Change to Directory where this Script Lies
scriptDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(scriptDir)

# Create an Output Folder Directory
outputPath  = scriptDir  + '/Output'
mechOutPath = outputPath + '/Mechanism'
infoOutPath = outputPath + '/Information'
auxOutPath  = outputPath + '/Auxiliary'
if not os.path.exists(outputPath):
    os.mkdir(outputPath)
if not os.path.exists(mechOutPath):
    os.mkdir(mechOutPath)
if not os.path.exists(infoOutPath):
    os.mkdir(infoOutPath)
if not os.path.exists(auxOutPath):
    os.mkdir(auxOutPath)

# Start Message
print('Starting Mechanism Parsing...\n**************************************************************************\n')


#########################################################################################################################################################

#Check for actual species in your base mechanism 
actualitvSpecies = ReadSpeciesFromChemkin(itvMechPath) 
actualSpeciesDic,FullSpeciesDic = CreateDicOfActualSpecies(itvThermoPath,actualitvSpecies) 
ExtraMechSpecies = ReadSpeciesFromChemkin(ExtraMechPath)

if not updateThermo: 
   if noInc:
      extraitvSpecies = ReadSpeciesFromChemkin(ExtraMechPath)
   else: 
      temp,Dummy = CreateDicOfActualSpecies(itvThermoPath,ExtraMechSpecies) 
      CreateExtraSpeciesDicFile2(temp,ExtraInChIPath)
      extraDict = CreateDicOfExtraSpecies(ExtraInChIPath,ExtraMechSpecies)
else: 
   #CreateExtraSpeciesDicFile(ExtraInChIPath)
   extraDict = CreateDicOfExtraSpecies(ExtraInChIPath,ExtraMechSpecies)

#Create Dictionary storing species composition 
#ExtraSpeciesCompo = GetComposition(ExtraThermoPath) 
 

#Define the searchList as difference between the species in your base mechanism and the extra mechanism. This is useful when combining two reduced mechanisms.   
if flagCompare: 
   if noInc:
      searchList = [] 
      for s in extraitvSpecies: 
          if s not in actualitvSpecies: 
             searchList.append(s)
      #searchList = list(set(extraitvSpecies) - set(actualitvSpecies)) 
   else: 
      searchList = defineExtraBasedOnComparison(actualSpeciesDic,extraDict) 
   print('**************************************************************************\n')

#Define the searchList based on searchWord 
elif searchWord:
   searchList = []  
   for elem in ExtraMechSpecies: 
       if searchWord in elem: 
          searchList.append(elem) 

#Define list of Extra species from file 
else :
   searchList = readExtraSpeciesNames(searchListPath) 

print( "Adding " + str(len(searchList)) + " Species to Base Mechanism" ) 
#########################################################################################################################################################
#Find Extra Reactions 
ExtraReactions = findExtraReactionsFromList(ExtraMechPath, searchList,ExtraMechSpecies)

#Read All Thermo and Transport 
temp1 = [] 
temp2 = [] 
for species in searchList:
    temp1 =  temp1 + findThermoFromSearchWord(ExtraThermoPath, species) 
    temp2 = temp2 + findTransportFromSearchWord(ExtraTransPath, species, [],True) 
ExtraThermoData    = temp1.copy() 
ExtraTransportData = temp2.copy()

ExtraSpeciesList = searchList.copy() 
if Recursive: 
   alreadyAnalSpeciesInExtra = [] 
   neededSpeciesList = ExtraSpeciesList.copy() 
   endFlag = True
   count = 1  
   while endFlag:
         ExtraReactions = findExtraReactionsFromList(ExtraMechPath, neededSpeciesList,ExtraMechSpecies)
         temp = [] 
         for species in neededSpeciesList: 
              temp =  temp + extractSpecies(ExtraReactions, species, True,neededSpeciesList,ExtraMechSpecies)
         ExtraSpeciesListnotInList = list(dict.fromkeys(temp))
         extraDict = CreateDicOfExtraSpecies(scriptDir)
         readInChIDict(ExtraInChIPath,extraDict, ExtraSpeciesListnotInList)
         missingSpeciesList = checkInChIWithDic(actualSpeciesDic,ExtraInChIPath, [], ExtraSpeciesListnotInList,alreadyAnalSpeciesInExtra)
         alreadyAnalSpeciesInExtra = alreadyAnalSpeciesInExtra + missingSpeciesList  
         if not  missingSpeciesList: 
            endFlag=False

         if flagITVFull: 
            SpeciesNeitherInITVFull = checkInChIWithDic(FullSpeciesDic,ExtraInChIPath,[],missingSpeciesList,alreadyAnalSpeciesInExtra)
            temp = neededSpeciesList.copy()
            for elem in temp: 
                if (elem not in neededSpeciesList) and elem not in SpeciesNeitherInITVFull: 
                   missingSpeciesList.remove(elem) 
  
 
         neededSpeciesList = neededSpeciesList + missingSpeciesList
         if setIterNum > 0: 
            if count+1 > setIterNum: 
               endFlag=False 
            count = count +1 
else:  
   #Define list of Extra species not in List
   temp = []
   if not DummyMech:  
      for species in searchList: 
          temp =  temp + extractSpecies(ExtraReactions, species, True,searchList,ExtraMechSpecies)
      ExtraSpeciesListnotInList = list(dict.fromkeys(temp)) 
   else: 
      ExtraSpeciesListnotInList = [] 

   # Read and Extend the InChI-Dictionary
   if writeFiles:
       if not noInc:
          print('Checking for direct dependent species for ExtraSpecies reactions...')
          readInChIDict(ExtraInChIPath,extraDict, ExtraSpeciesListnotInList)
   else:
       print('Output is turned OFF.\n')
 
   missingSpeciesList = checkInChIWithDic(actualSpeciesDic, ExtraInChIPath, [], ExtraSpeciesListnotInList, []) 
   neededSpeciesList = ExtraSpeciesList + missingSpeciesList

# Write New Species List to File
speciesPerLine = 4
if writeFiles:
    with open(infoOutPath + '/SpeciesList.txt', 'w') as file:
        for i in range(0, len(neededSpeciesList), speciesPerLine):
            line = "      ".join(neededSpeciesList[i:i+speciesPerLine])
            file.write(line + "\n")
    print('SpeciesList containing all directly and not directly speciefied Extra species has been written.\n')

# Extend the Missing Species Thermo and Transport Data
missingThermoData = []
missingTransportData = []
if Recursive: 
   alreadyChecked =[] 
   for neededSpecies in neededSpeciesList: 
       missingThermoData = missingThermoData + findThermoFromSearchWord(ExtraThermoPath, neededSpecies)
       missingTransportData = missingTransportData + findTransportFromSearchWord(ExtraTransPath, neededSpecies,alreadyChecked,True) 
       alreadyChecked.append(neededSpecies) 

   neededThermoData = missingThermoData.copy()  
   neededTransportData = missingTransportData.copy()  
else: 
   for missingSpecies in missingSpeciesList: 
       missingThermoData = missingThermoData + findThermoFromSearchWord(ExtraThermoPath, missingSpecies)
       missingTransportData = missingTransportData + findTransportFromSearchWord(ExtraTransPath, missingSpecies,[],True)
   neededThermoData = ExtraThermoData + missingThermoData
   neededTransportData = ExtraTransportData + missingTransportData


# Write Thermo and Transport Data of Extra and Needed Species to File
if writeFiles:
    with open(infoOutPath + '/ThermoData.txt', 'w') as file:
        file.writelines("%s\n" % line for line in neededThermoData)
        print('ThermoData containing all Thermo data entries has been written.\n')
    with open(infoOutPath + '/TransportData.txt', 'w') as file:
        file.writelines("%s\n" % line for line in neededTransportData)
        print('TransportData containing all Transport data entries has been written.\n')

# Extend the Missing Species Chemistry
missingSpeciesReactions = []
if Recursive:
   for missingSpecies in neededSpeciesList: 
       newEntry=findMissingReactions(ExtraMechPath, missingSpecies, "Obsoleta", missingSpeciesReactions)
       if newEntry not in missingSpeciesReactions:
          missingSpeciesReactions = missingSpeciesReactions + newEntry
   neededReactions = ExtraReactions + missingSpeciesReactions
   if setIterNum==0:
      relevantReactions = neededReactions.copy()
   else: 
      temp = [] 
      for species in neededSpeciesList: 
          temp =  temp + extractSpecies(neededReactions, species, True,neededSpeciesList,ExtraMechSpecies)
      secondGenSpeciesList = list(dict.fromkeys(temp))
      if writeFiles:
        print('Checking for second order dependent species for Extra reactions...')
        readInChIDict(ExtraInChIPath,extraDict, secondGenSpeciesList)

      with open(infoOutPath + '/NeededReactionTest.txt', 'w') as file:
        file.writelines("%s\n" % line for line in neededReactions)
        
      # Check if All Species from the Extra Mechanism are Represented in the ITV Mechanism and Delete Missing Species in 2nd Generation
     
      deprecatedSpeciesList =checkInChIWithDic(actualSpeciesDic,ExtraInChIPath,neededSpeciesList,secondGenSpeciesList,[])
      # Only Use Reactions which Do not Contain Deprecated Species
      relevantReactions, deprecatedReactions = relevantDeprecatedReactions(neededReactions, deprecatedSpeciesList)
      # Write List of Deprecated Species and Reactions
      if writeFiles:
          with open(auxOutPath + '/DeprecatedSpecies.txt', 'w') as file:
             file.writelines("%s\n" % line for line in deprecatedSpeciesList)
             print('DeprecatedSpecies containing list of all deleted species from Extra mechanism has been written.\nA total of ' + str(len(deprecatedSpeciesList)) + ' species has been deleted.\n')
          with open(auxOutPath + '/DeprecatedReactions.txt', 'w') as file:
             file.writelines("%s\n" % line for line in deprecatedReactions)
             print('DeprecatedReactions containing list of all deleted reactions from Extra mechanism has been written.\nA total of ' + str(len(deprecatedReactions)) + ' reactions has been deleted.\n')
          
else: 
   for missingSpecies in missingSpeciesList:
       newEntry=findMissingReactions(ExtraMechPath, missingSpecies, "Obsoleta", missingSpeciesReactions)
       if newEntry not in missingSpeciesReactions:
          missingSpeciesReactions = missingSpeciesReactions + newEntry 

   #Remove duplicate between Extra and missingSpeciesReactions 
   DummySpecReac= missingSpeciesReactions.copy() 
   for elem in DummySpecReac:  
       if elem in ExtraReactions:
          missingSpeciesReactions.remove(elem)  
           
   neededReactions = ExtraReactions + missingSpeciesReactions
   # Read All Participating second generation extraSpecies from the Extra Reactions List (Second Generation Dependency)
   temp = []
   if not DummyMech:  
      for species in searchList:
          temp =  temp + extractSpecies(neededReactions, species, True,searchList,ExtraMechSpecies)
      secondGenSpeciesList = list(dict.fromkeys(temp))
      if writeFiles:
         if not noInc: 
            print('Checking for second order dependent species for Extra reactions...')
            readInChIDict(ExtraInChIPath,extraDict, secondGenSpeciesList)
   else: 
      secondGenSpeciesList = [] 
   # Check if All Species from the Extra Mechanism are Represented in the ITV Mechanism and Delete Missing Species in 2nd Generation
   deprecatedSpeciesList=checkInChIWithDic(actualSpeciesDic,ExtraInChIPath,neededSpeciesList,secondGenSpeciesList,[])
   if flagITVFull: 
         SpeciesNeitherInITVFull = checkInChIWithDic(FullSpeciesDic,ExtraInChIPath,[],neededSpeciesList,[])
         temp = neededSpeciesList.copy()
         for elem in temp: 
             if (elem not in searchList) and (elem not in SpeciesNeitherInITVFull): 
                 neededSpeciesList.remove(elem) 
                 deprecatedSpeciesList.append(elem) 

   # Only Use Reactions which Do not Contain Deprecated Species
   relevantReactions, deprecatedReactions = relevantDeprecatedReactions(neededReactions, deprecatedSpeciesList)
   # Write List of Deprecated Species and Reactions
   if writeFiles:
        with open(auxOutPath + '/DeprecatedSpecies.txt', 'w') as file:
             file.writelines("%s\n" % line for line in deprecatedSpeciesList)
             print('DeprecatedSpecies containing list of all deleted species from Extra mechanism has been written.\nA total of ' + str(len(deprecatedSpeciesList)) + ' species has been deleted.\n')
        with open(auxOutPath + '/DeprecatedReactions.txt', 'w') as file:
             file.writelines("%s\n" % line for line in deprecatedReactions)
             print('DeprecatedReactions containing list of all deleted reactions from Extra mechanism has been written.\nA total of ' + str(len(deprecatedReactions)) + ' reactions has been deleted.\n')


# Write Original Reactions to File
if writeFiles:
    with open(auxOutPath + '/ReactionsExtraNames.txt', 'w') as file:
        file.writelines("%s\n" % line for line in relevantReactions)
        print('ReactionsExtra containing with Extra naming convention has been written.\n')

# Assign the Names of the Species According the the ITV Mechansim
if Recursive: 
   speciesNamesList = assignNames(actualSpeciesDic, ExtraInChIPath, neededSpeciesList)
else: 
   speciesNamesList = assignNames(actualSpeciesDic, ExtraInChIPath,ExtraMechSpecies)

# Print the Assigned Names
if writeFiles:
    with open(auxOutPath + '/AssignedSpeciesNames.txt', 'w') as file:
        file.write('Extra Names    ITV Names\n')
        for key, value in speciesNamesList.items():
            col1 = '{:<16}'.format(key)
            col2 = '{:<16}'.format(value)
            file.write(f'{col1}{col2}\n')
    print('AssignedNames containing the assigned names (Extra left, ITV right) has been written.\n')

# Replace the Names of the Species in the Extra-Reactions According the the ITV Mechansim
itvExtraReactions = replaceNames(relevantReactions, speciesNamesList)

# Write Modified Extra Reactions to File
if writeFiles:
    with open(infoOutPath + '/ReactionsITVnames.txt', 'w') as file:
        file.writelines("%s\n" % line for line in itvExtraReactions)
    print('ReactionsITVnames containing all Extra reactions with ITV naming convention has been written.\n')
   
# Copy Mechanism to Output Folder
if os.path.exists(mechOutPath + '/' + mech_name + '.mech'):
   os.remove(mechOutPath + '/' + mech_name + '.mech')
   os.remove(mechOutPath + '/' + mech_name + '.thermo')
   os.remove(mechOutPath + '/' + mech_name + '.trans')
try: 
   shutil.copy(itvMechPath, mechOutPath)
except shutil.SameFileError:
   pass
try: 
  shutil.copy(itvThermoPath, mechOutPath)
except shutil.SameFileError:
   pass
try: 
  shutil.copy(itvTransPath, mechOutPath)
except shutil.SameFileError:
   pass

os.rename(mechOutPath +'/' + mech_name_input + '.mech', mechOutPath + '/' + mech_name +'.mech')
os.rename(mechOutPath + '/' + mech_name_input + '.thermo', mechOutPath + '/' + mech_name + '.thermo')
os.rename(mechOutPath + '/' + mech_name_input + '.trans', mechOutPath + '/' + mech_name + '.trans')


# Write Species, Reactions, Thermo and Transport Data into ITV Mechanism
if writeFiles:
    insertTextIntoFile(mechOutPath + '/' + mech_name + '.mech', neededSpeciesList, 'SPECIES')
    insertTextIntoFile(mechOutPath + '/' + mech_name + '.mech', itvExtraReactions, 'REACTIONS')
    if updateThermo: 
       insertTextIntoFile(mechOutPath + '/' + mech_name + '.thermo', neededThermoData, 'THERMO')
       insertTextIntoFile(mechOutPath + '/' + mech_name + '.trans', neededTransportData, ' ')
    print('The Extra Reactions have been integrated into the ITV mechanism and the new mechanism has been \nwritten to the Mechanism folder.\n')


# Finalize
print('**************************************************************************\nThe mechanism parsing is finished.')
