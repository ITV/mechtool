import shutil
import re
import os


#########################################################################################################################################################
#Convert file containing Extra Species name to a list 
def readExtraSpeciesNames(file_path):
    species_list = []
    try:
        with open(file_path, 'r') as file:
            for line in file:
                print(line) 
                species_names = re.split(r',|\t|\s+', line.strip())
                for species_name in species_names:
                    if species_name:
                       print(species_name) 
                       species_list.append(species_name.strip())
    except FileNotFoundError:
        print("File not found.")
    
    return species_list
             

# Parser Function to Find Reactions from ExtraMechanism 
def findExtraReactionsFromSearchWord(path, word):
    with open(path,'r') as file:
        reactions = []
        flag = False
        for line in file:
            if line.startswith('!*'):
                continue
            if ('REACTIONS' in line) or ('reactions' in line):
                flag = True
            if (word in line) and (flag):
                reactions.append(line.strip())
    return reactions

def findExtraReactionsFromListOld(path, listOfSpec):
    with open(path,'r') as file:
        lines=file.readlines() 
        reactions = []
        flag = False
        reaction_pattern = re.compile(r'(.+?)\s*(?:=>|->|<=>|=)\s*(.+)')
        for i in range(len(lines)):
            if re.match(r'^(?:(?!\s*?)!|\s*!)',lines[i]):
               continue
            if ('REACTIONS' in lines[i]) or ('reactions' in lines[i]):
               flag = True
            if flag:
                match = reaction_pattern.match(lines[i])
                print(lines[i]) 
                if match: 
                   speciesinReac=extractSpeciesSingleReaction(lines[i],ExtraMechSpecies)
                   if CheckCommon(listOfSpec,speciesinReac):  
                      reactions.append(lines[i].strip())
                      if (i<len(lines)-1) and ('PLOG' in lines[i+1]):
                          j = i
                          while ('PLOG' in lines[j+1]):
                              reactions.append(lines[j+1].strip())
                              j = j + 1
                      if (i<len(lines)-1) and ('PLOG' in lines[i+2]) and ('DUPLICATE' in lines[i+1]):
                          reactions.append(lines[i+1].strip())
                          j = i
                          while ('PLOG' in lines[j+2]):
                              reactions.append(lines[j+2].strip())
                              j = j + 1
                      if (i<len(lines)-1) and ('DUPLICATE' in lines[i+1]):
                          reactions.append(lines[i+1].strip())
                          reactions.append(lines[i+2].strip())
                      if (i<len(lines)-1) and ('(+M)' in lines[i] or '+M' in lines[i]):
                          j = i+1 
                          match = reaction_pattern.match(lines[j]) 
                          while not match and j<len(lines)-1:
                                reactions.append(lines[j].strip())
                                j=j + 1 
    return reactions

def findExtraReactionsFromList(path, listOfSpec,ExtraMechSpecies):
    with open(path,'r') as file:
        lines=file.readlines() 
        reactions = []
        flag = False
        reaction_pattern = re.compile(r'(.+?)\s*(?:=>|->|<=>|=)\s*(.+)')

        count_tb = 0 
        count_tb_lat = 0 
        for i in range(len(lines)):
            if "(+M)" in lines[i]:
                count_tb +=1  
            if re.match(r'^(?:(?!\s*?)!|\s*!)',lines[i]):
               continue
            if ('REACTIONS' in lines[i]) or ('reactions' in lines[i]):
               flag = True
            if lines[i].strip() == 'END': 
               flag = False 
            if flag:
                match = reaction_pattern.match(lines[i])
                if match:
                   if "(+M)" in lines[i]: 
                       count_tb_lat +=1  
                   speciesinReac=extractSpeciesSingleReaction(lines[i],ExtraMechSpecies)
                   if CheckCommon(listOfSpec,speciesinReac):
                      string = lines[i].strip() + '\n' 
                      if lines[i+1]:   
                          j = i+1  
                          while not (reaction_pattern.match(lines[j])) and (j<len(lines)-1): 
                             if not lines[j].strip() == 'END'and not lines[j].strip().startswith('!'): 
                                string = string + lines[j]  
                             j = j + 1 
                      
                      reactions.append(string)
        if not count_tb == count_tb_lat: 
           diff = count_tb - count_tb_lat  
           print('Mechanism parser missed ' + str(diff) + ' reactions') 
           quit()  
    return reactions

# Parser Function to Find Reactions from Extra Mechanism
def findMissingReactions(path, word, searchWord,checkList):
    with open(path,'r') as file:
        lines = file.readlines()
        reactions = []
        flag = False
        reaction_pattern = re.compile(r'(.+?)\s*(?:=>|->|<=>|=)\s*(.+)')
        for i in range(len(lines)):
            if re.match(r'^(?:(?!\s*?)!|\s*!|END|\s*?END)',lines[i]):
               continue
            if ('REACTIONS' in lines[i]) or ('reactions' in lines[i]):
                flag = True
            if lines[i].strip() == 'END': 
                flag = False 
            if flag: 
                match = reaction_pattern.match(lines[i])
                if match: 
                   if (word in lines[i]) and (searchWord not in lines[i]) and (flag):
                       string = lines[i].strip() + "\n"
                       if lines[i+1]:   
                          j = i+1  
                          while not (reaction_pattern.match(lines[j])) and (j<len(lines)-1):
                             if not lines[j].strip() == 'END'and not lines[j].strip().startswith('!'): 
                               string = string + lines[j]   
                             j = j + 1
                       if string not in checkList:  
                          reactions.append(string)
        return reactions


# Parser Function to Extract Species associated Thermo Data
def findThermoFromSearchWord(path, word):
    with open(path, 'r', encoding='ISO-8859-15') as file:
        data = []
        for line in file:
            if line.startswith('!*'): 
                continue
            if word in line:
                data.append(line.strip())
                next_line = file.readline().strip()
                if 'C' in next_line:
                    data.append(next_line)
                    data.extend(file.readline().strip() for _ in range(3))
                else:
                    data.append(next_line)
                    data.extend(file.readline().strip() for _ in range(2))
    return data


# Parser Function to Extract Species associated Transport Data
def findTransportFromSearchWord(path, word, alreadyChecked,flagList):
    with open(path, 'r', encoding='ISO-8859-15') as file:
        data = []
        for line in file:
            if line.startswith('!*') or (line.isspace() or not line.strip()):
                continue
            else: 
               splitLine = line.split()
               spec_name = splitLine[0].strip()
               if flagList:
                  if word == spec_name and not spec_name in alreadyChecked: 
                     data.append(line.strip())
               else: 
                  if word in spec_name :
                     data.append(line.strip())
    return data


# Parser Function to Extract Species from Reactions
def extractSpecies(reactions, word, flag,List,ExtraMechSpecies):
    species = []
    for reaction in reactions:

        if '!' in reaction: 
            match = re.search(r"^.*?(?=!)",reaction )
            splitReaction = re.findall(r'\b(?:\d*\.?\d+\s*)?[A-Za-z0-9_*(),\-]+(?![A-Za-z0-9_*()#,\-])', match.group(0)) 
        else: 
            splitReaction = re.findall(r'\b(?:\d*\.?\d+\s*)?[A-Za-z0-9_()*,\-]+(?![A-Za-z0-9_*()#,\-])', reaction) 
        for formula in splitReaction:
            index = next((i for i, c in enumerate(formula) if not c.isdigit() and not c in ['.','E','-', 'e','/']), len(formula))
            spec = formula[index:]
            if re.findall(r"(?:\d+\.)?\w[\w\._()*#,\-]*", spec):
               spec_list = re.findall(r"(?:\d+\.)?\w[\w\._()*#,\-]*", spec)
               if not spec_list[0].isdigit():
                  sp = spec_list[0].strip()
                  if sp.endswith('('): 
                     sp = sp[:-1]
                  if List:
                     if sp not in List and (sp not in species) and ('PLOG' not in sp) and ('DUPLICATE' not in sp) and ('TROE' not in sp) and ('LOW' not in sp):
                        if not sp == 'END' and not sp == 'M)'and not sp == 'M' and not sp == 'CHEB' and not sp == 'TCHEB' and not sp == 'PCHEB':
                           if sp not in ExtraMechSpecies: 
                              print('Problem with mechanism parser. Found species ' + sp + ' not in extra mech in reaction ' + reaction ) 
                              quit() 
                           species.append(sp)
                  else: 
                     if (word in sp)==flag and (sp not in species) and ('PLOG' not in sp) and ('DUPLICATE' not in sp) and ('TROE' not in sp) and ('LOW' not in sp):
                        if not sp == 'END' and not sp == 'M' and not sp == 'CHEB' and not sp == 'TCHEB' and not sp == 'PCHEB':
                           if sp not in ExtraMechSpecies: 
                              print('Problem with mechanism parser. Found species ' + sp + ' not in extra mech in reaction' + reaction ) 
                              quit() 
            
                           species.append(sp)
 
    if '' in species:
        species.remove('')
    return species

def extractSpeciesSingleReaction(reaction,ExtraMechSpecies):
    species = []
    splitReaction = re.findall(r'\b(?:\d*\.?\d+\s*)?[A-Za-z0-9_*(),\-]+(?![A-Za-z0-9_*()#,\-])', reaction)
    checkFlag = False  
    for formula in splitReaction:
        index = next((i for i, c in enumerate(formula) if not c.isdigit() and not c in ['.','E','-', 'e','/']), len(formula))
        spec = formula[index:]
        if re.findall(r"(?:\d+\.)?\w[\w\.*()#,\-]*", spec):
           spec = re.findall(r"(?:\d+\.)?\w[\w\.*()#,\-]*", spec)
           if not spec[0].isdigit():
              spec = spec[0].strip()
              if spec.endswith('('): 
                 spec = spec[:-1]
              if spec in ExtraMechSpecies: 
                 checkFlag = True 
              if spec not in species and not re.match(r'^(END|\s*?END)',spec) and not spec == '(+M)' and not spec == 'M':
                 species.append(spec)
    if not checkFlag: 
       print("None of the species in " + reaction + " belongs to the list of ExtraSpecies") 
       quit() 
    if '' in species:
        species.remove('')
    return species

# Parser/Write Function to Compare and Extend the InChI-Dictionary
def readInChIDictOld(path, speciesList):
    newEntries = 0
    with open(path + '/InChIDictExtra.txt', 'a+') as file:
        lines = file.readlines()
        file.seek(0, 2)
        for species in speciesList:
            for line in lines:
                split = re.split('\s+', line)
                if not species.strip() == split[0].strip():
                    col1 = '{:<16}'.format(species)
                    col2 = '{:<16}'.format('InChI=')
                    file.write(f'{col1}{col2}\n')
                    newEntries = newEntries + 1
    if newEntries != 0: 
       print(str(newEntries) + ' new entries have been made to the InChIDict. Please add the corresponding InChI manually and re-run the program.\n')
    else: 
       print('No new entries have been made to the InChIDict\n')
    return 0

def readInChIDict(path,ExtraDic, speciesList):
    newEntries = 0
    for species in speciesList:
                if not species.strip() in list(ExtraDic.keys()):
                    with open(path, 'a+') as file:
                         file.seek(0, 2)
                         col1 = '{:<16}'.format(species)
                         col2 = '{:<16}'.format('InChI=')
                         file.write(f'{col1}{col2}\n')
                         newEntries = newEntries + 1
                         ExtraDic[species.strip] = 'InChI=' 
                    file.close() 
    if newEntries != 0: 
       print(str(newEntries) + ' new entries have been made to the InChIDict. Please add the corresponding InChI manually and re-run the program.\n')
    else: 
       print('No new entries have been made to the InChIDict\n')
    return 0
   
# Parser/Write Function to Check if All Species from the Polimi Mechanism are Represented in the ITV Mechanism
def checkInChIWithDic(thermoDic, inchiPath, nonValidGroup, validGroup,alreadyChecked):
    missingSpecies = []
    speciestoAddFromITVFull = [] 
    with open(inchiPath , 'r') as inchiFile:
        inchiLines = inchiFile.readlines()
    for inchiLine in inchiLines:
        split = re.split('\s+', inchiLine)
        if (split[1].strip() not in list(thermoDic.keys())) and (split[0] not in nonValidGroup) and (split[0] in validGroup):
           if (split[0].strip() not in alreadyChecked): 
              missingSpecies.append(split[0])
    if '' in missingSpecies:
        missingSpecies.remove('')
    return missingSpecies

def checkInChIWithDicITVFull(thermoDic,listOfSpec,alreadyChecked):
    missingSpecies = []
    speciestoAddFromITVFull = [] 
    for elem in listOfSpec: 
        if (elem.strip() not in list(thermoDic.values())):
           if (elem.strip() not in alreadyChecked): 
              missingSpecies.append(elem.strip())
    if '' in missingSpecies:
        missingSpecies.remove('')
    return missingSpecies

def checkInChI(thermoDir, inchiPath, nonValidGroup, validGroup):
    missingSpecies = []
    with open(thermoDir, 'r') as thermoFile:
        thermoLines = thermoFile.readlines()
    with open(inchiPath , 'r') as inchiFile:
        inchiLines = inchiFile.readlines()
    for inchiLine in inchiLines:
        split = re.split('\s+', inchiLine)
        count = 0
        for i in range(len(thermoLines)-1):
            if (split[1] not in thermoLines[i]) or (thermoLines[i+1].startswith('!!')):
                count = count + 1
            if (count == len(thermoLines)-1) and (split[0] not in nonValidGroup) and (split[0] in validGroup):
                missingSpecies.append(split[0])
    if '' in missingSpecies:
        missingSpecies.remove('')
    return missingSpecies


# Sorting Function to Assign Names of the ITV Mechanism to the Polimi Species
def assignNames(itvDir, inchiPath, missingList):
    with open(inchiPath , 'r') as inchiFile:
        inchiLines = inchiFile.readlines()
    speciesNames = {}
    for i in range(0, len(inchiLines), 1):
        ExtraName  = inchiLines[i].strip().split()[0]
        ExtraInchi = inchiLines[i].strip().split()[1]
        if ExtraInchi in list(itvDir.keys()):
           speciesNames[ExtraName.strip()] = itvDir[ExtraInchi] 
    return speciesNames


# Function to Find Relevant and Deprecated Reactions

def relevantDeprecatedReactions(neededReactions, deprecatedSpeciesList):
    relevantReactions = neededReactions.copy() 
    deprecatedReactions = []
    for reaction in neededReactions:
        count = 0
        flag = True
        for species in deprecatedSpeciesList:
            if (species in reaction):
                deprecatedReactions.append(reaction)
                if reaction in relevantReactions:  
                   relevantReactions.remove(reaction)
 
    return relevantReactions, deprecatedReactions

#Functions to Define Deprecated Reactions based on Species Composition 
def RemoveSpeciesBasedOnComposition (compositionInput, speciesList, compositionDic):   
   removedSpecies = [] 
   for species in speciesList: 
       temp = [] 
       for i in range(len(compostionInput)): 
           temp.append(setCondition(i, compositionDic,species,compositionInput))  
           if temp: 
              removedSpecies.append(temp[0])
 
   removedSpecies = list(dict.fromkeys(removedSpecies))
   return removedSpecies 
 


def GetComposition (thermo): 
    CompositionDic = {} 
    try: 
     f = open(thermo, 'r')
    except IOError:
     print("Couldn't open thermo file ")
    therm = f.readlines()
    f.close() 
    
    newfile = [] 
    for line in therm:
        line = line.rstrip()
        if "!" in line and not re.match(r"^\!",line): 
            line = line.split("!", 1)[0]
            newfile.append(line) 
        else: 
            newfile.append(line) 
                
    i = 0 

    while i < (len(newfile)-1):
          if re.search(r"[1\s+]$",newfile[i]) and not re.match(r"^\!",therm[i]):  
             line1 = newfile[i];line1=line1.strip(); 
             #get species 
              
             if (re.match(r"^([^\s]+)([\D\d]+)$",line1)): 
                 l = re.search(r"^([^\s]+)([\D\d]+)$",line1)
                 spec_name = l.group(1)
                 spec_comp = line1[24:44] 
                 spec_name = spec_name.strip()
             else: 
                exit("No species composition found") 

             print(spec_name)              
             CompositionDic[spec_name] = [-1,-1,-1,-1,-1,-1] 
                 
             match = re.findall(r"([a-zA-Z])\s+([0-9]+)",spec_comp)
             for item in match: 
                   #atom = match.group(1);num = match.group(2);
                   if re.search(r"C|c",item[0]): 
                      CompositionDic[spec_name][0] = item[1] 
                   elif re.search(r"O|o",item[0]):
                      CompositionDic[spec_name][1] = item[1]
                   elif re.search(r"H|h",item[0]):
                      CompositionDic[spec_name][2] = item[1] 
                   elif re.search(r"N|n",item[0]):
                      CompositionDic[spec_name][3] = item[1] 
                   elif re.search(r"AR|ar",item[0]): 
                      CompositionDic[spec_name][4] = item[1]
                   elif re.search(r"HE|he",item[0]): 
                      CompositionDic[spec_name][5] = item[1]
             i+=1
          i+=1 
def SetConditon (compoIndex, speciesCompo,speciesName,  compositionInput): 
  
   if speciesCompo[speciesName][compoIndex] > compositionInput[compoIndex]:
      return speciesName 
             
           
# Sorting Function to Replace Names of the Extra Mechanism to the ITV Species
def replaceNames(reactions, speciesMap):
    itvReactions = []
    for reaction in reactions:
        splitReaction = re.split('(\+|=>|<=>|\s+|=)', reaction)
        for i in range(len(splitReaction)):
            flag = True
            index = next((j for j, c in enumerate(splitReaction[i]) if not c.isdigit() and not c in ['.', '-', 'e','/']), len(splitReaction[i]))
            formula = splitReaction[i][index:].strip()
            if formula.endswith('('):
               formula = formula[:-1] 
            if formula in list(speciesMap.keys()) and (flag):
               splitReaction[i] = splitReaction[i].replace(formula, speciesMap[formula])
               flag = False
        itvReactions.append(''.join(splitReaction))
    return itvReactions

#Utility functions  
def ReadSpeciesFromChemkin(file_path):
    species_list = []
    try:
        with open(file_path, 'r') as file:
            lines = file.readlines() 
            for i in range(len(lines)-1):
                line = lines[i].strip()
                if line.startswith('SPECIES'):
                   j=i+1
                   while not lines[j].startswith('END'):
                        if not lines[j].startswith('!') or (line.isspace() or not line.strip()): 
                            if '!' in lines[j]: 
                                match = re.search(r"^.*?(?=!)",lines[j] )
                                line = match.group(0)
                                species = line.split() 
                            else: 
                                species = lines[j].split()
                            for s in species: 
                               species_list.append(s.strip())
                        j=j+1 
    except FileNotFoundError:
        print("File not found.")
    
    return species_list

def CreateDicOfActualSpecies(path,speciesList):
    itvInchiDic={}
    itvInchiDicFull={}
    pattern = r"InChI=1S\/[^ ]+"    
    with open(path, 'r') as thermoFile:
         thermoLines = thermoFile.readlines()
         for i in range(len(thermoLines)-1):
             if "InChI=" in thermoLines[i]:
                 match = re.search(pattern, thermoLines[i]) 
                 InchiExpres = match.group()
                 j = i+1 
                 l = re.search(r"^([^\s]+)([\D\d]+)$",thermoLines[j])
                 spec_name = l.group(1)
                 spec_name = spec_name.strip() 
                 if not spec_name.startswith('!'): 
                    itvInchiDicFull[InchiExpres.strip()]=spec_name.strip()       
                 if spec_name.strip() in speciesList:
                    itvInchiDic[InchiExpres.strip()]=spec_name.strip()       
    return itvInchiDic,itvInchiDicFull          

 
def CreateDicOfExtraSpecies(path,ExtraSpecies):
    ExtraInchiDict={}
    with open(path , 'r') as inchiFile:
        inchiLines = inchiFile.readlines()
    for inchiLine in inchiLines:
        split = re.split('\s+', inchiLine)
        if split[0].strip() in ExtraSpecies: 
           ExtraInchiDict[split[0].strip()] = split[1].strip()
    for species in ExtraSpecies: 
        if species not in list(ExtraInchiDict.keys()): 
           print("Add species " + species + " to Extra InchiI Dictionary")  
   
    return ExtraInchiDict          

def CreateExtraSpeciesDicFile(path):
    ExtraInchiDict={}
    with open(path , 'r') as inchiFile:
        inchiLines = inchiFile.readlines()
    inchiFile.close() 
    for inchiLine in inchiLines:
        split = re.split('\s+', inchiLine)
        if (len(split) >2): 
            ExtraInchiDict[split[0].strip()] = split[2].strip()
        else:  
            ExtraInchiDict[split[0].strip()] = split[1].strip()
     
    with open(path, 'w') as file:
         for key, value in ExtraInchiDict.items():
            col1 = '{:<16}'.format(key)
            col2 = '{:<16}'.format(value)
            file.write(f'{col1}{col2}\n')
    file.close() 

def CreateExtraSpeciesDicFile2(dic,path):
    with open(path, 'w') as file:
          for key, value in dic.items():
            col1 = '{:<16}'.format(value)
            col2 = '{:<16}'.format(key)
            file.write(f'{col1}\t{col2}\n')
    file.close() 
 
def CheckCommon(list1, list2):
    set1 = set(list1)
    set2 = set(list2)
    return len(set1.intersection(set2)) > 0

def getImportantSpecies(path, actualSpeciesDic,fuelSpecificSpecies,RedMechSpecies):  

    ExtraSpec = [] 
    ExtraInchiDict={}
    with open(path , 'r') as inchiFile:
        inchiLines = inchiFile.readlines()
    inchiFile.close() 
    for inchiLine in inchiLines:
        split = re.split('\s+', inchiLine)
        ExtraInchiDict[split[0].strip()] = split[1].strip()

    for spec in RedMechSpecies: 
        if spec not in fuelSpecificSpecies:
           if spec in list(ExtraInchiDict.keys()): 
              if ExtraInchiDict[spec].strip() not in list(actualSpeciesDic.keys()):
                 print("Found additional species to add " + spec + " " + ExtraInchiDict[spec.strip()] ) 
                 ExtraSpec.append(spec) 
           else: 
             print("Missing InChI for " + spec + " Please add it to the InchiDictionary and re-run the programme") 
    return ExtraSpec 

def defineExtraBasedOnComparison(BaseDict, ExtraDict):
    searchListComp = []  
    for spec in ExtraDict.keys(): 
        if ExtraDict[spec].strip() not in list(BaseDict.keys()):
           searchListComp.append(spec) 
    return searchListComp 


def createSublists(big_list, sublist_size):
    sublists = []
    n = len(big_list)
    num_sublists = n // sublist_size

    for i in range(num_sublists):
        start = i * sublist_size
        end = (i + 1) * sublist_size
        sublist = big_list[start:end]
        sublists.append(sublist)

    remaining = big_list[num_sublists * sublist_size:]
    if remaining:
        sublists.append(remaining)

    return sublists

def cleanUpThermoData(neededThermoData,speciesName): 
    flagDone = False 
    thermo= neededThermoData.copy() 
    for i in range(0,len(thermo)):
        if "!" in thermo[i] and not re.match(r"^\!",thermo[i]):
            line = thermo[i].split("!", 1)[0] 
        else: 
            line = thermo[i]   
        if re.search(r"[1\s+]$",line) and not re.match(r"^\!",line):
           if (re.match(r"^([^\s]+)([\D\d]+)$",line)):
              l = re.match(r"^([^\s]+)([\D\d]+)$",line) 
              if speciesName == l.group(1) and not flagDone: 
                 flagDone = True
                 continue  
              if speciesName == l.group(1) and flagDone: 
                 neededThermoData.remove(thermo[i])
                 j=i+1  
                 limit = j+3 
                 while j< len(thermo)-1 and j<limit: 
                        neededThermoData.remove(thermo[j])
                        j = j +1  
           
def cleanUpTransData(neededTransData,speciesName): 
    flagDone = False 
    trans= neededTransData.copy() 
    for i in range(0,len(trans)):
        if "!" in trans[i] and not re.match(r"^\!",trans[i]):
            line = trans[i].split("!", 1)[0] 
        else: 
            line = trans[i]   
        if not re.match(r"^(\#|!|END)",line) and re.match(r"[^\s]+",line): 
           temp = line.split()
           spec_name = temp[0]
           if speciesName == spec_name and not flagDone:
              flagDone = True 
              continue 
           if speciesName == spec_name and flagDone:
              neededTransData.remove(trans[i])

def insertTextIntoFile(filePath, stringList, keyword):
    with open(filePath, 'r') as file:
        lines = file.readlines()
    for i, line in enumerate(lines):
        if keyword in line:
            lines[i+1:i+1] = ['\n!!!! ADDED FOR THE EXTRA MODEL !!!!\n']
            if keyword == 'SPECIES': 
               listOfSpec = createSublists(stringList,8)         
               lines[i+2:i+2] = ['\t'.join(map(str,sublist)) + '\n' for sublist in listOfSpec]
               lines[i+2+len(listOfSpec):i+2+len(listOfSpec)] = ['!!!! END OF EXTRA MODEL INSERT !!!!\n\n']
            elif keyword == 'REACTIONS':        
               lines[i+2:i+2] = [s for s in stringList]
               lines[i+3+len(stringList):i+3+len(stringList)] = ['\n!!!! END OF EXTRA MODEL INSERT !!!!\n\n']
            else:
               lines[i+4:i+4] = [s + '\n' for s in stringList]
               lines[i+4+len(stringList):i+3+len(stringList)] = ['!!!! END OF EXTRA MODEL INSERT !!!!\n\n']
 
            break
    with open(filePath, 'w') as file:
        file.writelines(lines)
    return 0

