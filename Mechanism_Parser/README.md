#Mechanism Parser Options 

Main functionality: Combine two different chemical kinetic model (A,B)  

The combination can be done in different ways: 
    1-Specify a list of species that has to be extracted from model B. The reactions from model B in which the specified species appear either as reactants or as products will be added to mechanism A  
    2-Extract the species that are in model B and not in A. The identification of these species can be done based on the InChIs or on the species name. Unless you don't want to combine sub-modules of the same 
      kinetic model, the first option is always preferred 
    3-Extract the species from model B with a prefix in their name. The reactions presenting these species either as reactants or products will be extracted and combined with mechanims A    
    
##NOTE:
When using option 1 and 3 some extracted reactions might lead to the introduction of new additional species (in addition to the ones speciefied by the user). 
These species and related reactions are dected and added to model A as well. 
This can be done: 
  -Iteratively. You can specify the number of iterations 
  
 
    
    
