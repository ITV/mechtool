import os

def merge_and_remove_duplicates(input_folder, output_file):
    try:
        unique_lines = set()  # Set to store unique lines

        # Open the output file in write mode
        with open(output_file, 'w') as output:
            # Iterate over each file in the input folder
            for file_name in os.listdir(input_folder):
                # Check if the file is a text file
                if file_name.endswith('.txt'):
                    file_path = os.path.join(input_folder, file_name)
                    
                    # Read the content from the current file
                    with open(file_path, 'r') as file:
                        lines = file.readlines()

                        # Iterate over each line in the file
                        for line in lines:
                            # Check if the line is not already in the set
                            if line not in unique_lines:
                                # Write the line to the output file
                                output.write(line)
                                # Add the line to the set to track uniqueness
                                unique_lines.add(line)

        print(f"Files in {input_folder} successfully merged and duplicates removed in {output_file}")
    except Exception as e:
        print(f"An error occurred: {e}")

# Example usage:
input_folder_path = 'SpeciesList/'
output_file_path = 'ExtractedSpeciesList.txt'

merge_and_remove_duplicates(input_folder_path, output_file_path)
