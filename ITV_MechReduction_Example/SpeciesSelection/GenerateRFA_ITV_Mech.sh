#!usr/bash

### Either enter the tolerance when prompted
#echo 'Define the limit: '
#read tol

### Or delcare it here
# Tolerances for fuel and PAH
tol_fuel=0.01
tol_pah=0.05
echo 'tol. fuel is: ' $tol_fuel
echo 'tol. pah  is: ' $tol_pah

### Either enter the input file (ScanMan output) when prompted
echo 'Define the input file (out..):'
#read outFile

### Or declare it here
outFile="outscan"
echo 'ScanMan rate file is: ' $outFile

### Either enter the rate containing file when prompted
#echo 'Define the rate containing file: '
#read ReacRateFile

### Or declare a list here
#declare -a FileList=("ReacRate_C2H4_p01phi100to1111.dout" "ReacRate_C2H4_p01phi100to1250.dout" "ReacRate_C2H4_p01phi100to1428.dout")
declare -a FileList=("ReacRates.dout")

# subdir name
subdir="Plots"
subdir_files="SpeciesList"

mkdir $subdir
mkdir $subdir_files

for filename in ${FileList[@]}; do
	
	ReacRateFile=$filename
	echo 'Processing file: ' $ReacRateFile
	# output-filename
	outfile="${ReacRateFile::-5}_"
	
	#######################
	#### RFA EXECUTION ####
	#######################
	
	### FUEL AND COMBUSTION PRODUCTS
	declare -a SpeciesList_1=("C2H4" "A2CH3" "CH4" "CO" "CO2" "H" "H2" "O2" "OH" "CO" "H2O" "C2H" "C2H2")
	tol=$tol_fuel
	
	for val in ${SpeciesList_1[@]}; do
		echo $val
		spec=$val
	
		case="c"
		out=$subdir/"$outfile$spec"_"$case"_"$tol.png"
		out_file_species=$subdir_files/"$outfile$spec"_"$case"_"$tol.txt"
		./reacFlux_main.py -i $outFile -r $ReacRateFile -t $spec -s all -l $tol -f $case -w $out_file_species
		mv networkx_graph.png $out
		
		case="p"
		out=$subdir/"$outfile$spec"_"$case"_"$tol.png"
		out_file_species=$subdir_files/"$outfile$spec"_"$case"_"$tol.txt"
		./reacFlux_main.py -i $outFile -r $ReacRateFile -t $spec -s all -l $tol -f $case -w $out_file_species
		mv networkx_graph.png $out
	done
	
	############# PAH ############
	declare -a SpeciesList_2=("A1" "A2" "A2R5")
	tol=$tol_pah
	
	for val in ${SpeciesList_2[@]}; do
		echo $val
		spec=$val
	
		case="c"
		out=$subdir/"$outfile$spec"_"$case"_"$tol.png"
		out_file_species=$subdir_files/"$outfile$spec"_"$case"_"$tol.txt"
		./reacFlux_main.py -i $outFile -r $ReacRateFile -t $spec -s all -l $tol -f $case -w $out_file_species
		mv networkx_graph.png $out
		
		case="p"
		out=$subdir/"$outfile$spec"_"$case"_"$tol.png"
		out_file_species=$subdir_files/"$outfile$spec"_"$case"_"$tol.txt"
		./reacFlux_main.py -i $outFile -r $ReacRateFile -t $spec -s all -l $tol -f $case -w $out_file_species
		mv networkx_graph.png $out
	done
done

