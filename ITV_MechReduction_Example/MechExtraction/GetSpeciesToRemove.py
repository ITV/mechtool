# remove_elements.py

def read_file(file_path):
    with open(file_path, 'r') as file:
        content = file.readlines()
    return content

def write_file(file_path, content):
    with open(file_path, 'w') as file:
        file.writelines(content)

def remove_elements(file1_path, file2_path, output_path):
    # Read content of both files
    file1_content = read_file(file1_path)
    file2_content = read_file(file2_path)

    # Remove elements of file2 from file1
    updated_content = [line for line in file1_content if line not in file2_content]

    # Write the updated content to the output file
    write_file(output_path, updated_content)

if __name__ == "__main__":
    # Specify the paths of the two files and the output file
    file1_path = "ListFullSpec.txt"
    file2_path = "ExtractedSpeciesList.txt"
    output_path = "SpeciesToRemove.txt"

    # Remove elements of file2 from file1 and save the result to the output file
    remove_elements(file1_path, file2_path, output_path)

    print(f"Elements from {file2_path} removed from {file1_path}. Results saved to {output_path}.")

