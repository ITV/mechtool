#### How to use the reaction flux analysis tool to reduce a mechanism ###
	
#### Reduced mechanism species list extraction
	
	- When compiling the starting mechanism, use the option -sr3>outscan to obtain the reaction rate file from ScanMan
	- Get the ReactRates.dout files from the simulations of interest by adding the appropriate flags in FlameMaster (AdditionalOutput is TRUE)
	- Run the bash script "GenerateRFA_ITV_Mech.sh". Modify the tolerances, react rate and scanman output file names accordingly
	- Once all the RFA habe been run, execute the MergeSpeciesList.py script. This will merge all the text files inside "SpeciesList" into a single file "ExtractedSpeciesList.txt"
	- which contains all the required species for the reduced mech.

#### Extraction of submechanism

	- Provide a full list of the species of the starting mechanism (In one column)
	- Run the GetSpeciesToRemove.py script. Rename the filenames in the script accordingly
	- Run the ExtractSubMechanism.py script as: "python3 ExtractSubMechanism.py MECH_FILE.mech SpeciesToRemove.txt"
	- The extracted mechanism is in "ModifiedSubMechanism/MergedMechanism.mech"
	- The species list at the beginning of the mech file needs to be replaced with the set in "ExtractedSpeciesList.txt"
	- Further manual modifications might be required for species appearing in the third body reactions (e.g., HE, AR, CH3OH,...)
	


